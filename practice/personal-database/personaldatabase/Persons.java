package personaldatabase;

import java.util.List;
import java.util.ArrayList;
import java.util.Random;

import java.util.SortedSet;
import java.util.TreeSet;

import java.time.LocalDate;

public class Persons
{
    private final List<Person> persons;
    private final int [] idPool;
    private int curLimit;
    private final int cnt;
    private final int bornFrom;
    private final int bornTo;
    private final Random random;
    //----------------------------------------------------------------------------------------------
    Persons ( int cnt, int bornFrom, int bornTo ) throws Exception
    {
        this . cnt = cnt;
        this . bornFrom = bornFrom;
        this . bornTo = bornTo;
        curLimit = cnt;
        persons = new ArrayList<>();
        idPool = new int [cnt];
        random = new Random ();

        generatePersons ();
    }
    //----------------------------------------------------------------------------------------------
    public List<Person> getPersons ()
    {
        return persons;
    }
    //----------------------------------------------------------------------------------------------
    public void printPersons ()
    {
        for ( Person p : persons )
            System . out . println ( p );
    }
    //----------------------------------------------------------------------------------------------
    private void generatePersons () throws Exception
    {
        for ( int i = 0; i < cnt; ++i )
        {
            RandomDate dob = new RandomDate ( bornFrom, bornTo );

            // LocalDate ... using a factory method for constructing the object
            // https://stackoverflow.com/questions/38424319
            persons . add ( new Person (    generateId (),
                                            generateName (),
                                            LocalDate . of ( dob . year, dob . month, dob . day ),
                                            chooseSex (),
                                            chooseJob (),
                                            generateSalary ( 5000, 50000 ) ) );
        }
    }
    //----------------------------------------------------------------------------------------------
    private String generateName ()
    {
        return generateCapAltString ( 3, 10 ) + " " + generateCapAltString ( 3, 10 );
    }
    //----------------------------------------------------------------------------------------------
    // generate capitalized alternating string
    private String generateCapAltString ( int minLen, int maxLen )
    {
        int length = random . nextInt ( maxLen + 1 ) + minLen;    // 3 to 10 chars long
        StringBuilder sb = new StringBuilder ();
        sb . append ( toUpper ( generateConsonant () ) );

        for ( int i = 1; i < length; ++i )
        {
            if ( i % 2 == 0 ) // even
                sb . append ( generateConsonant () );
            else
                sb . append ( generateVowel () );
        }

        return sb . toString ();
    }
    //----------------------------------------------------------------------------------------------
    private char toUpper ( char c )
    {
        if ( c > 96 && c < 123 )
            c -= 32;

        return c;
    }
    //----------------------------------------------------------------------------------------------
    private char generateConsonant ()
    {
        final char [] consonants = "bcdfghjklmnpqrstvwxz" . toCharArray ();

        return consonants [random . nextInt ( consonants . length )];
    }
    //----------------------------------------------------------------------------------------------
    private char generateVowel ()
    {
        final char [] vowels = "aeiouy" . toCharArray ();

        return vowels [random . nextInt ( vowels . length )];
    }
    //----------------------------------------------------------------------------------------------
    // generate a unique id
    // chosen from the idPool which "shrinks" with every iteration
    // linear time - no collision
    private int generateId ()
    {
        int retval;

        int rand = random . nextInt ( curLimit ); // generate the index

        if ( idPool [rand] == 0 )
        {
            retval = rand;
        }
        else
        {
            retval = idPool [rand];
        }

        -- curLimit;

        if ( idPool [curLimit] == 0 )
            idPool [rand] = curLimit;
        else
            idPool [rand] = idPool [curLimit];

        return retval;
    }
    //----------------------------------------------------------------------------------------------
    private Sex chooseSex ()
    {
        Sex [] values = Sex . values ();
        return values [random . nextInt ( values . length )];
    }
    //----------------------------------------------------------------------------------------------
    private Job chooseJob ()
    {
        Job [] values = Job . values ();
        return values [random . nextInt ( values . length )];
    }
    //----------------------------------------------------------------------------------------------
    private int generateSalary ( int from, int to )
    {
        return random . nextInt ( to - from + 1 ) + from;
    }
    //----------------------------------------------------------------------------------------------
    public static void main ( String [] args ) throws Exception
    {
        new Persons ( 1000, 1950, 2000 ) . printPersons ();
    }
}

package personaldatabase;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JDialog;
import javax.swing.table.DefaultTableModel;

import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.util.List;
import java.util.ArrayList;
import java.util.SortedSet;
import java.util.Scanner;

import java.io.File;
import java.io.IOException;

class Gui
{
    // -1 = no table row chosen
    private int curRow = -1;

    String tableData [][];

    JTextField  idField,
                nameField,
                dobField,
                sexField,
                jobField,
                salaryField;

    Gui ( String currLang, List<Person> employees )
    {
        JFrame frame = new JFrame ( "Employee database" );
        frame . setDefaultCloseOperation ( JFrame . EXIT_ON_CLOSE );
        frame . setSize ( 1000, 800 );

        String tableHeader [] = fillHeader ( currLang );
        tableData = fillTable ( employees );

        // make the table read-only
        DefaultTableModel tableModel = new DefaultTableModel ( tableData, tableHeader )
        {
            @Override
            public boolean isCellEditable ( int row, int column ) {
                return false;
            }
        };

        frame . getContentPane () . setLayout ( new BorderLayout () );

        JTable table = new JTable ( tableModel );
        JScrollPane scrollPane = new JScrollPane ( table );

        JLabel idLabel = new JLabel ( "ID", JLabel . CENTER );
        idField = new JTextField ();
        JLabel nameLabel = new JLabel ( "Name", JLabel . CENTER );
        nameField = new JTextField ();
        JLabel dobLabel = new JLabel ( "Date of birth", JLabel . CENTER );
        dobField = new JTextField ();
        JLabel sexLabel = new JLabel ( "Sex", JLabel . CENTER );
        sexField = new JTextField ();
        JLabel jobLabel = new JLabel ( "Job", JLabel . CENTER );
        jobField = new JTextField ();
        JLabel salaryLabel = new JLabel ( "Salary", JLabel . CENTER );
        salaryField = new JTextField ();

        JButton saveButton = new JButton ( "Save changes" );

        JPanel panel = new JPanel ( new GridLayout ( 0, 2 ) );

        table . getSelectionModel () . addListSelectionListener ( new ListSelectionListener ()
        {
            @Override
            public void valueChanged ( ListSelectionEvent e )
            {
                curRow        = table . getSelectedRow ();
                String id     = table . getValueAt ( curRow, 0 ) . toString ();
                String name   = table . getValueAt ( curRow, 1 ) . toString ();
                String dob    = table . getValueAt ( curRow, 2 ) . toString ();
                String sex    = table . getValueAt ( curRow, 3 ) . toString ();
                String job    = table . getValueAt ( curRow, 4 ) . toString ();
                String salary = table . getValueAt ( curRow, 5 ) . toString ();

                idField     . setText ( id );
                nameField   . setText ( name );
                dobField    . setText ( dob );
                sexField    . setText ( sex );
                jobField    . setText ( job );
                salaryField . setText ( salary );
            }
        });

        saveButton.  addActionListener ( new ActionListener ()
        {
            @Override
            public void actionPerformed ( ActionEvent e )
            {
                updateTable ( tableModel );
            }
        });

        frame . getContentPane () . add ( scrollPane, BorderLayout . NORTH );

        panel . add ( idLabel );
        panel . add ( idField );
        panel . add ( nameLabel );
        panel . add ( nameField );
        panel . add ( dobLabel );
        panel . add ( dobField );
        panel . add ( sexLabel );
        panel . add ( sexField );
        panel . add ( jobLabel );
        panel . add ( jobField );
        panel . add ( salaryLabel );
        panel . add ( salaryField );

        frame . getContentPane () . add ( panel,  BorderLayout . CENTER );
        frame . getContentPane () . add ( saveButton, BorderLayout . SOUTH );

        frame . setVisible ( true );
    }
    //----------------------------------------------------------------------------------------------
    /*
        Get the header keys from the input file into the list.
    */
    private List<String> getHeaderKeys ( String filename )
    {
        List<String> headerKeys = new ArrayList<>();

        try
        {
            Scanner sc = new Scanner ( new File ( filename ) );

            while ( sc . hasNextLine () )
            {
                String line = sc . nextLine ();
                headerKeys . add ( line );
            }
            sc . close ();
        }
        catch ( IOException e )
        {
            e . printStackTrace ();
        }

        return headerKeys;
    }
    //----------------------------------------------------------------------------------------------
    /*
        Get the header values according to the selected language.
    */
    private String [] fillHeader ( String currLang )
    {
        List<String> headerKeys = getHeaderKeys ( "header.conf" );

        String [] headerValues = new String [headerKeys . size ()];

        for ( int i = 0; i < headerKeys . size (); ++i )
        {
            headerValues [i] = new Lang ( currLang ) . get ( headerKeys . get ( i ) );
        }

        return headerValues;
    }
    //----------------------------------------------------------------------------------------------
    private String [][] fillTable ( List<Person> employees )
    {
        String [][] out = new String [employees . size ()][6]; // id, name, dob, sex, job, salary

        int i = 0;
        for ( Person p : employees )
        {
            out [i][0] = String . valueOf ( p . getId () );
            out [i][1] = p . getName ();
            out [i][2] = String . valueOf ( p . getDob () );
            out [i][3] = String . valueOf ( p . getSex () );
            out [i][4] = String . valueOf ( p . getJob () );
            out [i][5] = String . valueOf ( p . getSalary () );
            ++i;
        }

        return out;
    }
    //----------------------------------------------------------------------------------------------
    private void updateTable ( DefaultTableModel tableModel )
    {
        // if a row is selected - otherwise exception
        if ( curRow > -1 )
        {
            updateTableData ();
            updateTableModel ( tableModel );
        }
    }
    //----------------------------------------------------------------------------------------------
    private void updateTableData ()
    {
        tableData [curRow][0] = idField . getText ();
        tableData [curRow][1] = nameField . getText ();
        tableData [curRow][2] = dobField . getText ();
        tableData [curRow][3] = sexField . getText ();
        tableData [curRow][4] = jobField . getText ();
        tableData [curRow][5] = salaryField . getText ();
    }
    //----------------------------------------------------------------------------------------------
    // manual solution - wrong ???
    private void updateTableModel ( DefaultTableModel tableModel )
    {
        for ( int i = 0; i < 6; ++i )
            tableModel . setValueAt ( tableData [curRow][i], curRow, i );
    }
}

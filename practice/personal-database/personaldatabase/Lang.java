package personaldatabase;

import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class Lang
{
    //----------------------------------------------------------------------------------------------
    private final Map<String, String> headerMap; // map of the table header according to a given language
    private final String filename; // name of the language file
    //----------------------------------------------------------------------------------------------
    Lang ( String currLang )
    {
        headerMap = new HashMap<>();
        filename = currLang . concat ( ".lang" );
        fillHeaderMap ();
    }
    //----------------------------------------------------------------------------------------------
    public String get ( String key )
    {
        if ( ! headerMap . containsKey ( key ) )
            return "(undefined)";

        return headerMap . get ( key );
    }
    //----------------------------------------------------------------------------------------------
    /*
        Fill the header by parsing the input file.
    */
    private void fillHeaderMap ()
    {
        try
        {
            Scanner sc = new Scanner ( new File ( filename ) );

            while ( sc . hasNextLine () )
            {
                String line = sc . nextLine ();
                String [] parts = line . split ( "," );
                try
                {
                    headerMap . put ( parts [0], parts [1] );
                }
                catch ( Exception e )
                {
                    System . err . println (    "ERROR - bad format of the \""
                                                + filename
                                                + "\" language file." );
                    System . exit ( 1 );
                }
            }
            sc . close ();
        }
        catch ( IOException e )
        {
            e . printStackTrace ();
            System . exit ( 1 );
        }
    }
}

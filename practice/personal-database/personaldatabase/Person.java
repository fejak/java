package personaldatabase;

import java.time.LocalDate;
import java.util.Comparator;
//==================================================================================================
public class Person
{
    private final int id;
    private final LocalDate dob;
    private final String name;
    private final Sex sex;
    private final Job job;
    private final int salary;

    Person ( int id, String name, LocalDate dob, Sex sex, Job job, int salary ) throws Exception
    {
        this . id = checkNonNegativeInt ( id, "Id" );
        this . name = name;
        this . dob = dob;
        this . sex = sex;
        this . job = job;
        this . salary = checkNonNegativeInt ( salary, "Salary" );
    }

    private int checkNonNegativeInt ( int number, String str ) throws Exception
    {
        if ( number < 0 )
            throw new Exception ( str + " must be non-negative." );
        return number;
    }

    public int getId ()
    {
        return id;
    }

    public LocalDate getDob ()
    {
        return dob;
    }

    public String getName ()
    {
        return name;
    }

    public Sex getSex ()
    {
        return sex;
    }

    public Job getJob ()
    {
        return job;
    }

    public int getSalary ()
    {
        return salary;
    }

    @Override
    public String toString ()
    {
        return  getId ()
                + ", "
                + getName ()
                + ", "
                + getDob ()
                + ", "
                + getSex ()
                + ", "
                + getJob ()
                + ", "
                + getSalary ();
    }

    public static void main ( String [] args ) throws Exception
    {
        //Person p = new Person ( "John", 1979, 10, 5, Sex . MALE, Job . ACCOUNTANT, 10000 );
    }
}
//==================================================================================================
class PersonComparatorId implements Comparator<Person>
{
    public int compare ( Person p1, Person p2 )
    {
        return p1 . getId () - p2 . getId ();
    }
}
//==================================================================================================
class PersonComparatorName implements Comparator<Person>
{
    public int compare ( Person p1, Person p2 )
    {
        return p1 . getName () . compareTo ( p2. getName () );
    }
}

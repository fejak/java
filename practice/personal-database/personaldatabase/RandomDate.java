package personaldatabase;

import java.util.Random;

public class RandomDate
{
    private static final int [] days = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    private static final int [] daysLeap = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

    public final int year;
    public final int month;
    public final int day;

    RandomDate ( int yearMin, int yearMax )
    {
        year = generateYear ( yearMin, yearMax );
        month = generateMonth ();
        day = generateDay ();
    }

    private int generateYear ( int min, int max )
    {
        return generateInt ( min, max );
    }

    private int generateMonth ()
    {
        return generateInt ( 1, 12 );
    }

    private int generateDay ()
    {
        if ( checkLeap ( year ) )
        {
            return generateInt ( 1, daysLeap [month - 1] );
        }
        else
        {
            return generateInt ( 1, days [month - 1] );
        }
    }

    private boolean checkLeap ( int year )
    {
        if ( year % 400 == 0 )
            return true;
        else
        if ( year % 100 == 0 )
            return false;
        else
        if ( year % 4 == 0 )
            return true;
        else
            return false;
    }

    private int generateInt ( int min, int max )
    {
        return new Random () . nextInt ( max + 1 - min ) + min;
    }

    // test
    public static void main ( String [] args )
    {
        for ( int i = 0; i < 100; ++i )
        {
            RandomDate date = new RandomDate ( 1950, 2000 );
            System . out . println ( date . year + "-" + date . month + "-" + date . day );
        }
    }
}

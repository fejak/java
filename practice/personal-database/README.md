# Personal Database

This is a simple Java program which generates a list of persons (employees) and displays them in a Swing table.

## To compile & run (from this directory)

    javac -Xlint:all personaldatabase/Start.java
    java personaldatabase.Start

## To create a JAR file (from this directory)

    javac -Xlint:all personaldatabase/Start.java
    jar cfmv personalDatabase.jar manifest personaldatabase/*.class header.conf en.lang

## To run the JAR file

    java -jar personalDatabase.jar

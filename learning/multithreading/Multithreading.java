/*
    Java - Basics of multithreading

    Author: gitlab.com/fejak
*/
//--------------------------------------------------------------------------------------------------
/*
    Multithreading is a native part of the Java language.
    In the past it used to be implemented using the "green threads" (which exist only
    inside the JVM and the OS is not aware of them), now it's implemented using the underlying
    native threads in the OS, so it can actually utilize more CPU cores. (?)
    Multithreading in JVM is implemented using the POSIX threads (pthreads) on the Unix-like OS's
    and the Windows threads on the Windows OS's.

    There are two ways to start using threads in Java:
    1) implement the Runnable interface (this option is preferred)
    2) extend the Thread class

    Inheritance implies specialization. (by adding more properties we specify the parent class more)
    We generally don't want to specialize the "Thread" class, we just want to use it.
    https://stackoverflow.com/questions/541487/implements-runnable-vs-extends-thread-in-java

    The main thread is implicitly started by the JVM which starts executing the Main-Class's
    main () method.
*/
//--------------------------------------------------------------------------------------------------
/*
    implements Runnable => composition ("HAS-A")

    This solution is preferred over "extends Thread".
    - Allows to inherit from another class
    - Looser coupling
    - Many threads can share the same class instance

    The Runnable interface contains only the run () method header.
*/
class ImplementsRunnable implements Runnable
{
    // this method defines the new thread code
    @Override
    public void run ()
    {
        for ( int i = 0; i < 10; ++i )
            System . out . println ( "1" );
    }
}
//--------------------------------------------------------------------------------------------------
/*
    extends Thread => inheritance ("IS-A")

    The Thread class itself implements the Runnable interface.
*/
class ExtendsThread extends Thread
{
    // this method defines the new thread code
    @Override
    public void run ()
    {
        for ( int i = 0; i < 10; ++i )
            System . out . println ( "2" );
    }
}
//--------------------------------------------------------------------------------------------------
public class Multithreading
{
    private static void CallImplementsRunnable ()
    {
        // we can't call the start () method directly - first we must instantiate the Thread class
        ImplementsRunnable ir = new ImplementsRunnable ();
        // we can run many threads with the same object instance
        new Thread ( ir ) . start ();
        new Thread ( ir ) . start ();
        new Thread ( ir ) . start ();
        new Thread ( ir ) . start ();
    }

    private static void CallExtendsThread ()
    {
        // we can call the start () method directly, because ExtendsThread IS Thread
        // we must instantiate a new object for every new thread
        new ExtendsThread () . start ();
    }

    // the main thread starts here
    public static void main ( String [] args )
    {
        // the order of execution of newly started threads is non-deterministic
        CallImplementsRunnable ();
        CallExtendsThread ();
    }
}
